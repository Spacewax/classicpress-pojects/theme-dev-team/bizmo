<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bizmo
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-info">

		<p>
			<?php echo bizmo_copyright(); ?> <a href="https://spacewax.net">Spacewax Studios</a>
		</p>
		<p>
			<!--<a href="<?php echo esc_url( __( 'https://classicpress.net/', 'bizmo' ) ); ?>">-->
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'bizmo' ), '<a href="https://classicpress.net/">ClassicPress</a>' );
				?>
			<!--</a>-->
			<span class="sep"> | </span>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'bizmo' ), 'bizmo', '<a href="https://spacewax.net">JMColeman</a>' );
				?>
		</p>

		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
